package aplicacion;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

public class App {

	public static void main(String[] args) {
//		System.out.println("---------------- PROPERTIES ----------------");
//		try {
//			usandoProperties();
//		} catch (IOException ex) {
//			System.err.println(ex);
//		}

		System.out.println("---------------- PREFERENCES ----------------");
		try {
			usandoPreferences();
		} catch (BackingStoreException | IOException ex) {
			System.err.println(ex);
		}

	}

	private static void usandoProperties() throws FileNotFoundException, IOException {

		// Acceso individual a una propiedad del sistema
		System.out.println("Carpeta de usuario: " + System.getProperty("user.home"));
		// Otras properties:
		// https://docs.oracle.com/javase/tutorial/essential/environment/sysprop.html
		System.out.println("-----------------------------------");

		// Acceso a todas las propiedades del sistema
		Properties ps = System.getProperties();
		// Mostrar todas las propiedades
		ps.list(System.out);
		System.out.println("-----------------------------------");

		// Podriamos guardarlas en un fichero
		String ruta = System.getProperty("user.home") + System.getProperty("file.separator");
		File f = new File(ruta + "prueba.properties");
		ps.store(new FileOutputStream(f), "Propiedades de sistema");

//		// Creación de propiedades personales
		Properties p1 = new Properties();
		p1.setProperty("usuario", "sergio");
		p1.setProperty("opcion1", "A");
		p1.setProperty("opcion2", "B");
		p1.list(System.out);
		System.out.println("-----------------------------------");
		p1.store(new FileOutputStream(f), "Mis propiedades");

		// Lectura del fichero de las propiedades personales
		Properties p2 = new Properties();
		// Cargamos las propiedades desde el fichero a memoria
		p2.load(new FileInputStream(f));
		p2.list(System.out);
		System.out.println("-----------------------------------");
		System.out.println("El valor de la clave 'opcion1' es: " + p2.getProperty("opcion1"));
		System.out.println("La opcion2 es: " + p2.getProperty("opcion2"));

		// Recorrer todas las claves de una en una:
		Enumeration<Object> claves = p2.keys();
		while (claves.hasMoreElements()) {
			String c = (String) claves.nextElement();
			System.out.println(c + " = " + p2.getProperty(c));
		}

		// Modificación
		p2.setProperty("usuario", "sergi");
		p2.setProperty("opcion3", "C");
		p2.setProperty("opcion4", "D");

		// Borrado
		p2.remove("opcion4");
		p2.store(new FileOutputStream(f), "Mis propiedades");
	}

	private static void usandoPreferences() throws BackingStoreException, FileNotFoundException, IOException {
		// Los archivos de preferencias pueden estar ubicados en la ruta de sistema o de
		// usuario
		// Ruta de sistema: requiere permisos.
//        Preferences pf = Preferences.systemRoot();
//        Preferences pf = Preferences.systemNodeForPackage(Principal.class);

		// Ruta de usuario: diferentes formas de crear archivos de preferencias.
//        Preferences pf = Preferences.userRoot();
//        Preferences pf = Preferences.userNodeForPackage(App.class);
		Preferences pf = Preferences.userRoot().node("miprograma");

		pf.put("usuario", "sergio");
		pf.put("opcion1", "A");
		pf.put("opcion2", "B");
		pf.putBoolean("opcion3", true);
		pf.putInt("opcion4", 50);

		// Si una opción no existe
		String valorOpcion222 = pf.get("opcion2222", null);
		if (valorOpcion222 != null)
			System.out.println("Existe. Su valor es: " + valorOpcion222);
		else
			System.out.println("No existe");
		// Podemos darle valor un valor por defecto en caso de que no exista:
		System.out.println("Opcion222 es: " + pf.get("opcion222", "Ojo! Sin definir"));

		String[] keys = pf.keys();
		for (String k : keys) {
			System.out.print(k + " - ");
			System.out.println(pf.get(k, null));
		}

		// Podemos borrar una clave
		pf.remove("opcion2");
		// O modificarla
		pf.putBoolean("opcion3", false);

		// O podemos borrar todo el nodo completo
//		pf.removeNode();
	}

}
